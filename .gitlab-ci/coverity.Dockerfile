FROM fedora:36

ARG COVERITY_SCAN_PROJECT_NAME
ARG COVERITY_SCAN_TOKEN

RUN curl https://scan.coverity.com/download/cxx/linux64 -o /tmp/cov-analysis-linux64.tgz --form project="${COVERITY_SCAN_PROJECT_NAME}" --form token="${COVERITY_SCAN_TOKEN}"
RUN tar xfz /tmp/cov-analysis-linux64.tgz
RUN rm /tmp/cov-analysis-linux64.tgz

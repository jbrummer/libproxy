/* px-module.c
 *
 * Copyright 2022-2023 Jan-Michael Brummer
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "px-module.h"
#include "px-config-module.h"
#include "px-pacrunner-module.h"

G_DEFINE_QUARK (px - module - error - quark, px_module_error)

struct _PxModule {
  GObject parent_instance;
};

G_DEFINE_TYPE (PxModule, px_module, G_TYPE_OBJECT)

static void
px_module_class_init (PxModuleClass *klass)
{
}

static void
px_module_init (PxModule *self)
{
}

PxModule *
px_module_load_finished (GObject       *source_object,
                         GAsyncResult  *result,
                         GError       **error)
{
  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
load_module_ready_cb (GFile        *target,
                      GAsyncResult *result,
                      gpointer      user_data)
{
  GTask *load_task = user_data;
  g_autoptr (PxModule) module = NULL;
  g_autoptr (GError) error = NULL;

  module = g_task_propagate_pointer (G_TASK (result), &error);
  if (error) {
    g_task_return_error (load_task, g_steal_pointer (&error));
    return;
  }

  g_task_return_pointer (load_task, g_steal_pointer (&module), NULL);
}


static void
load_module_thread (GTask        *task,
                    gpointer      source_object,
                    gpointer      task_data,
                    GCancellable *cancellable)
{
  GFile *target = source_object;
  GModule *module;
  gpointer func;
  PxModuleCreate create;
  PxModule *px_module;

  g_debug ("Loading %s...\n", g_file_peek_path (target));
  module = g_module_open (g_file_peek_path (target), G_MODULE_BIND_LAZY);
  if (!module) {
    g_warning ("%s: failed for %s", __FUNCTION__, g_file_peek_path (target));
    g_task_return_new_error (task, PX_MODULE_ERROR, PX_MODULE_ERROR_INVALID_MODULE, "Could not load module");
    return;
  }

  if (!g_module_symbol (module, "px_module_create", &func)) {
    g_warning ("Could not load module!");
    g_module_close (module);
  }

  g_module_make_resident (module);

  create = func;
  px_module = create ();

  if (!px_module) {
    g_warning ("%s: failed for %s", __FUNCTION__, g_file_peek_path (target));
    g_task_return_new_error (task, PX_MODULE_ERROR, PX_MODULE_ERROR_INVALID_MODULE, "Could not instantiate module");
    return;
  }

  g_print ("%s: Loaded %s\n", __FUNCTION__, g_file_peek_path (target));

  g_task_return_pointer (task, g_steal_pointer (&px_module), NULL);
}

PxModule *
px_module_load (GFile     *target,
                GFileInfo *info,
                gpointer   user_data)
{
  GModule *module;
  gpointer func;
  PxModuleCreate create;
  PxModule *px_module;

  g_assert (target);
  g_assert (info);

  g_debug ("%s: Loading %s\n", __FUNCTION__, g_file_peek_path (target));
  module = g_module_open (g_file_peek_path (target), G_MODULE_BIND_LAZY);
  if (!module) {
    g_warning ("%s: failed for %s", __FUNCTION__, g_file_peek_path (target));
    return NULL;
  }

  if (!g_module_symbol (module, "px_module_create", &func)) {
    g_warning ("Could not load module!");
    g_module_close (module);
  }

  g_module_make_resident (module);

  create = func;
  px_module = create ();

  if (!px_module) {
    g_warning ("%s: failed for %s", __FUNCTION__, g_file_peek_path (target));
    return NULL;
  }

  g_print ("%s: Loaded %s\n", __FUNCTION__, g_file_peek_path (target));
  return px_module;
}

void
px_module_load_async (GFile               *target,
                      GFileInfo           *info,
                      GCancellable        *cancellable,
                      GAsyncReadyCallback  callback,
                      gpointer             user_data)
{
  GTask *task;
  GTask *module_task;

  g_assert (target);
  g_assert (info);

  g_debug ("%s: Loading %s\n", __FUNCTION__, g_file_peek_path (target));
  task = g_task_new (target, cancellable, callback, user_data);
  g_task_set_return_on_cancel (task, TRUE);

  module_task = g_task_new (target, g_task_get_cancellable (task), (GAsyncReadyCallback)load_module_ready_cb, task);
  g_task_set_task_data (module_task, GUINT_TO_POINTER (TRUE), NULL);
  g_task_set_return_on_cancel (module_task, TRUE);
  g_task_run_in_thread (module_task, load_module_thread);
}

const char *
px_module_get_name (PxModule *self)
{
  if (PX_IS_CONFIG_MODULE (self)) {
    PxConfigModuleInterface *ifc = PX_CONFIG_MODULE_GET_INTERFACE (self);

    return ifc->name;
  }

  return NULL;
}

static void
px_config_module_interface_init (PxConfigModuleInterface *iface)
{
}

GType
px_config_module_get_type (void)
{
  static GType type = 0;
  static const GTypeInfo info = {
    sizeof (PxConfigModuleInterface),
    NULL,
    NULL,
    (GClassInitFunc)px_config_module_interface_init
  };

  if (type) return type;

  type = g_type_register_static (
    G_TYPE_INTERFACE,
    "PxConfigModule",
    &info,
    0);

  return type;
}


static void
px_pacrunner_module_interface_init (PxPacrunnerModuleInterface *iface)
{
}

GType
px_pacrunner_module_get_type (void)
{
  static GType type = 0;
  static const GTypeInfo info = {
    sizeof (PxPacrunnerModuleInterface),
    NULL,
    NULL,
    (GClassInitFunc)px_pacrunner_module_interface_init
  };

  if (type) return type;

  type = g_type_register_static (
    G_TYPE_INTERFACE,
    "PxPacrunnerModule",
    &info,
    0);

  return type;
}
